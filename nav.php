<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Mire se erdhet ne AlbiNET</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="dist/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="dist/css/carousel.css" rel="stylesheet">
  </head>
<!-- NAVBAR
================================================== -->
  <body>
    <div class="navbar-wrapper">
      <div class="container">

        <nav class="navbar navbar-inverse navbar-static-top">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="index.php">AlbiNET</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                
                <li  class="<?php if(basename($_SERVER['PHP_SELF'])=='index.php') echo "active"; ?>"><a href="index.php">Fillimi</a></li>
                <li class="dropdown <?php if(basename($_SERVER['PHP_SELF'])=='interneti.php' || basename($_SERVER['PHP_SELF'])=='televizioni.php' || basename($_SERVER['PHP_SELF'])=='paketa_duo.php') echo "active"; ?>" >
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Sherbimet <span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="interneti.php">Internet</a></li>
                    <li><a href="televizioni.php">Televizion</a></li>
                    <li><a href="paketa_duo.php">Paketa DUO</a></li>
                    
                  </ul>
				  <li class="<?php if(basename($_SERVER['PHP_SELF'])=='lokacionet.php') echo "active"; ?>"><a href="lokacionet.php">Lokacionet</a></li>
				  <li class="<?php if(basename($_SERVER['PHP_SELF'])=='rreth_albinet.php') echo "active"; ?>"><a href="rreth_albinet.php">Rreth AlbiNET</a></li>
				  <li class="<?php if(basename($_SERVER['PHP_SELF'])=='kontakt.php') echo "active"; ?>"><a href="kontakt.php">Na Kontakto</a></li>
                
                </li>
              </ul>
            </div>
          </div>
        </nav>

      </div>
    </div>
</body>