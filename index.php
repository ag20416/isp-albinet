
<?php include 'nav.php';?>


    <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="First slide">
          <div class="container">
            <div class="carousel-caption">
              <h1>6 Muaj + 6 Gratis </h1>
              <p> Parapaguaj Internet per 6 muaj dhe fito 6 muaj gratis </p>
			   <p><a class="btn btn-lg btn-primary" href="#" role="button">Meso me shume</a></p>
              </div>
          </div>
        </div>
        <div class="item">
          <img src="data:image/gif;base64,R0lGODlhAQABAIAAAGZmZgAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Second slide">
          <div class="container">
            <div class="carousel-caption">
              <h1>Oferta e Fundvitit</h1>
              <p>Realizo kontrat 2 vjeqare dhe perfito 15euro internet 10Mbps+TV 100+ Kanale </p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Meso me shume</a></p>
            </div>
          </div>
        </div>
        <div class="item">
          <img src="data:image/gif;base64,R0lGODlhAQABAIAAAFVVVQAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Third slide">
          <div class="container">
            <div class="carousel-caption">
              <h1>TV 3 Muaj</h1>
              <p>Perfito Televizion per 3 muaj me vetem 20Euro</p>
              <p><a class="btn btn-lg btn-primary" href="#" role="button">Meso me shume</a></p>
            </div>
          </div>
        </div>
      </div>
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div><!-- /.carousel -->


    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container marketing">

      <!-- Three columns of text below the carousel -->
      <div class="row">
        <div class="col-lg-4">
          <img class="img-circle" src="dist/image/interneti.png" alt="Generic placeholder image" style="width: 140px; height: 140px;">
          <h2>Interneti</h2>
          <p>Kemi përshpejtuar përsëri shpejtësinë e internetit për pakot ekzistuese, pa i ndryshuar çmimet.
Pakoja e internetit e cila deri më sot ofronte deri 5Mbps, tani për të njëjtin çmim ofrohet me 10Mbps, ndërsa edhe shpejtësia e internetit në pakon që ofronte 10Mbps, tani është rritur në 15Mbps.
Me ndryshimet e reja, shpejtësia e ofruar në pakot e internetit nga IPKO është nga 10Mbps deri në 80Mbps, që njëkohësisht është pakoja më e shpejtë e internetit në Kosovë.</p>
          <p><a class="btn btn-default" href="interneti.html" role="button">Meso me shume &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="img-circle" src="dist/image/tv.png" alt="Generic placeholder image" style="width: 140px; height: 140px;">
          <h2>Televizioni Digjital</h2>
          <p>Pako familjare është kombinim i kanaleve DigitAlb, kanaleve lokale të Kosovës, të Shqipërisë dhe kanaleve të mirënjohura ndërkombëtare. Kjo pako do t'ju shërbej më së miri për argëtimin e familjes tuaj dhe posaçërisht fëmijëve tuaj!.
          <br /> <br />Super Sport
IPKO së bashku me Digjitalb kanë të drejtat e transmetimit të ndeshjeve të kampionateve më prestigjioze evropiane, si dhe ato botërore në Kosovë.</p>
          <p><a class="btn btn-default" href="televizioni.html" role="button">Meso me shume &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="img-circle" src="dist/image/pakoja.png" alt="Generic placeholder image" style="width: 140px; height: 140px;">
          <h2>Paketa DUO</h2>
          <p>Interneti dhe televizioni digjital i kombinuar ne nje qasje te pershtatshme dhe me ekologjike per shfrytezuesit tane . 
          <br> Perfito nga oferta per te pranuar te dy keto sherbime te kombinuar dhe shume te pershtatshem per ju . <br> <br>

          Internet 10Mbps + Pako Familjare bashkarisht 
          </p>
          <p><a class="btn btn-default" href="paketa_duo.html" role="button">Meso me shume &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
      </div><!-- /.row -->


      <!-- START THE FEATURETTES -->

      <hr class="featurette-divider">

      <div class="row featurette">
        <div class="col-md-7">
          <h2 class="featurette-heading">AlbiNET edhe ne Prishtine <span class="text-muted">Kyqu tani</span></h2>
          <p class="lead">Sherbimi kabllor me internet dhe televizion tani vjen edhe ne Prishtine duke mundesuar komunikim te shpejt dhe shumellojshmeri te kanaleve televizive</p>
        </div>
        <div class="col-md-5">
          <img src="http://upload.wikimedia.org/wikipedia/commons/1/15/Prishtina_perspektiv_nga_katedrala.jpg"class="featurette-image img-responsive" data-src="holder.js/500x500/auto" alt="Generic placeholder image">
        </div>
      </div>

      <hr class="featurette-divider">

      <div class="row featurette">
        <div class="col-md-5">
          <img src="http://www.dreamboxbuy.eu/wp-content/uploads/2013/02/tring-digital-dreamboxbuy-500x270.png" class="featurette-image img-responsive" data-src="holder.js/500x500/auto" alt="Generic placeholder image">
        </div>
        <div class="col-md-7">
          <h2 class="featurette-heading">Shtohen 10 Kanale te reja <span class="text-muted">Tring PLUS</span></h2>
          <p class="lead">AlbiNET tani shton edhe 10 programe te reja ne platformen televizive duke e pasuruar edhe me shume </p>
        </div>
      </div>

      <hr class="featurette-divider">

      <div class="row featurette">
        <div class="col-md-7">
          <h2 class="featurette-heading">AlbiNET hap zyret edhe ne Peje<span class="text-muted">.</span></h2>
          <p class="lead">Hapet nje zyre e re per sherbimet e albinet .</p>
        </div>
        <div class="col-md-5">
          <img src="http://farm4.static.flickr.com/3204/2397933191_eb2014d548_b.jpg" class="featurette-image img-responsive" data-src="holder.js/500x500/auto" alt="Generic placeholder image">
        </div>
      </div>

      <hr class="featurette-divider">

      <!-- /END THE FEATURETTES -->


      
    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="dist/js/jquery.min.js"></script>
    <script src="dist/js/bootstrap.min.js"></script>
    <script src="dist/js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="dist/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>

<?php include 'footer.php';?>