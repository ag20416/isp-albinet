<?php include 'nav.php';?>


   <div class="container">

      <div class="blog-header">
        <h1 class="blog-title">Rreth Nesh/h1>
        <p class="lead blog-description"></p>
      </div>

      <div class="row">

        <div class="col-sm-8 blog-main">

          <div class="blog-post">
            <h2 class="blog-post-title">Rreth Nesh</h2>

            <p>2005 - Janar. AlbiNet - u hap si internet caffe e pare ne Prishtine e cila u sherbente edhe qytetarve te vendeve te ndryshem.</p>
            <hr>
            <p>2005 janar. AlbiNET Net – ofron shërbimin Interneti ne shtepi duke përdorur frekuencat valore (wireless) në Prishtine, disa muaj më vonë ofron dhe shërbime
si:servisim dhe shitje.</p>
        
              <p>2008 Mars. AlbiNET - ofron shërbim për telefonin VOIP 
              ( voice over IP)

            <p>
            2008 Janar. AlbiNET dhe Unioni Financiar Prishtinë – Dega Prishtine, nënshkruan marrëveshjen e përfaqësimit me Western Union Financial Services Inc, duke autorizuar Unionin Financiar Prishtinë për ofrimin e shërbimit Western Union – Transfetë Parash në Kosovë, më vonë Unioni Financiar Prishtinë ofron shërbime Union Net Arkëtime dhe Pagesa. </p>
            <p> 2009 Shkurt. AlbiNET fillon shtrirjen e rrjetës kabëllor HFC që qytetaret e këtij rajoni te marrin 3 shërbime permes një kabulli (Internet, TV, VOIP).</p>
<p>2010 Maj. AlbiNET zgjerohet edhe ne disa fshatra duke perdorur fiber si interkoneksion . </p>
<p> 2012 Gusht. AlbiNET bën shtrirjen e fibrit optic në të gjithë zonën e Prishtines për gjat magjistrales 01 duke rrespektuar licensen nga Ministria e infrastructures së Kosovës.</p>
<p> 2013 Mars. Dixhitalizon rrjeten kabëllore HFC dhe ofron shërbimin televiziv me STB (Reciever kabëllor) me buqet më të gjërë kanalesh.</p>

          </div><!-- /.blog-post -->

          

        </div><!-- /.blog-main -->

        
          
        
      </div><!-- /.row -->

    </div><!-- /.container -->

      <!-- /END THE FEATURETTES -->


      <!-- FOOTER -->
     

    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="dist/js/jquery.min.js"></script>
    <script src="dist/js/bootstrap.min.js"></script>
    <script src="dist/js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="dist/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
<?php include 'footer.php';?>