-- phpMyAdmin SQL Dump
-- version 4.2.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jan 04, 2015 at 10:21 PM
-- Server version: 5.5.38
-- PHP Version: 5.6.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `albinet`
--

-- --------------------------------------------------------

--
-- Table structure for table `klientet`
--

CREATE TABLE `klientet` (
`id` int(11) NOT NULL,
  `user` varchar(50) NOT NULL,
  `pass` varchar(40) NOT NULL,
  `emri` varchar(50) NOT NULL,
  `mbiemri` varchar(50) NOT NULL,
  `vendi` int(11) NOT NULL,
  `tel` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `sherbimi` int(11) NOT NULL,
  `cnr` varchar(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `klientet`
--

INSERT INTO `klientet` (`id`, `user`, `pass`, `emri`, `mbiemri`, `vendi`, `tel`, `email`, `sherbimi`, `cnr`) VALUES
(2, 'agashi', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Gashi', '', 1, '049 600 505', 'agashi5@live.com', 3, '01241548725'),
(3, 'agashi5', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'Avni', 'Gashi', 3, '044 123 456', '', 1, '06241548725');

-- --------------------------------------------------------

--
-- Table structure for table `kontakt`
--

CREATE TABLE `kontakt` (
`id` int(11) NOT NULL,
  `emrimbiemri` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `telefoni` varchar(50) NOT NULL,
  `mesazhi` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kontakt`
--

INSERT INTO `kontakt` (`id`, `emrimbiemri`, `email`, `telefoni`, `mesazhi`) VALUES
(1, 'Arben', 'test', '213123131231231', ''),
(2, 'awdawd', 'awdawd', '21312321313123', 'awdwad');

-- --------------------------------------------------------

--
-- Table structure for table `sherbimet`
--

CREATE TABLE `sherbimet` (
`id` int(11) NOT NULL,
  `emri` varchar(50) NOT NULL,
  `cmimi` float NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sherbimet`
--

INSERT INTO `sherbimet` (`id`, `emri`, `cmimi`) VALUES
(2, 'Internet', 15),
(3, 'Internet + TV', 20);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
`id` int(11) NOT NULL,
  `user` varchar(50) NOT NULL,
  `pass` varchar(40) NOT NULL,
  `emri` varchar(50) NOT NULL,
  `mbiemri` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `user`, `pass`, `emri`, `mbiemri`) VALUES
(1, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'Arbnor', 'Gashi');

-- --------------------------------------------------------

--
-- Table structure for table `vendet`
--

CREATE TABLE `vendet` (
`id` int(11) NOT NULL,
  `emri` varchar(50) NOT NULL,
  `lagja` varchar(150) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vendet`
--

INSERT INTO `vendet` (`id`, `emri`, `lagja`) VALUES
(1, 'Prishtinë', 'Lakrishte'),
(2, 'Prizren', ''),
(3, 'Gjakovë', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `klientet`
--
ALTER TABLE `klientet`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kontakt`
--
ALTER TABLE `kontakt`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sherbimet`
--
ALTER TABLE `sherbimet`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendet`
--
ALTER TABLE `vendet`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `klientet`
--
ALTER TABLE `klientet`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `kontakt`
--
ALTER TABLE `kontakt`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sherbimet`
--
ALTER TABLE `sherbimet`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `vendet`
--
ALTER TABLE `vendet`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;